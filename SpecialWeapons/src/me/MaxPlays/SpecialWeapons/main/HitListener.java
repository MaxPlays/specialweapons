package me.MaxPlays.SpecialWeapons.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class HitListener implements Listener {

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player && e.getEntity() instanceof Player){
			Player hit = (Player) e.getEntity();
			Player hitter = (Player) e.getDamager();
			ItemStack is = hitter.getItemInHand();
			if(is != null && Items.isSpecial(is)){
			
				if(SpecialWeapons.wg != null){
					ApplicableRegionSet set = SpecialWeapons.wg.getRegionManager(hitter.getWorld()).getApplicableRegions(hitter.getLocation());
					StateFlag.State state = set.queryState(null, DefaultFlag.PVP);
					if (state == null || state == StateFlag.State.DENY){
						if(!(SpecialWeapons.wg.getRegionManager(hitter.getWorld()).getApplicableRegions(hitter.getLocation()).size() <= 0))
							return;
					}
				}
	
				switch(Items.getSpecial(is)){
				case BA:
					hit.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 3*20, 0));
					break;
				case HA:
					return;
				case PA:
					hit.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 3*20, 0));
					break;
				case PS:
					hit.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 4*20, 0));
					break;
				default:
					return;
				}
			}
		}
	}
	
}
