package me.MaxPlays.SpecialWeapons.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;

public class DeathListener implements Listener {

	public static HashMap<String, ItemStack[]> map = new HashMap<>();
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDeath(PlayerDeathEvent e){
		Player p = (Player) e.getEntity();
		List<ItemStack> save = new ArrayList<>();
		List<ItemStack> drops = new ArrayList<>();
		for(ItemStack is: e.getDrops())
			drops.add(is);
		for(ItemStack is: drops){
			if(Items.isSpecial(is)){
				e.getDrops().remove(is);
			}
		}
		for(ItemStack is: p.getInventory().getContents()){
			if(is != null && Items.isSpecial(is)){
				save.add(is);
			}
		}
		if(map.containsKey(p.getName()))
			map.remove(p.getName());
		map.put(p.getName(), save.toArray(new ItemStack[]{}));
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRespawn(PlayerRespawnEvent e){
		Player p = e.getPlayer();
		if(map.containsKey(p.getName())){
			for(ItemStack i: map.get(p.getName()))
				p.getInventory().addItem(i);
			map.remove(p.getName());
		}
	}
	
	public static File file = new File("plugins/SpecialWeapons/saves.dat");
	public static File folder = new File("plugins/SpecialWeapons");
	
	public static void save(){
		try{
			if(!folder.exists())
				folder.mkdirs();
			if(file.exists())
				file.delete();
			file.createNewFile();
			
			FileOutputStream fos = new FileOutputStream(new File("plugins/SpecialWeapons/saves.dat"));
			BukkitObjectOutputStream boos = new BukkitObjectOutputStream(fos);
			boos.writeObject(map);
			boos.flush();
			fos.flush();
			boos.close();
			fos.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public static void load(){
		try{
			
			if(file.exists()){
				
				FileInputStream fis = new FileInputStream(file);
				BukkitObjectInputStream bois = new BukkitObjectInputStream(fis);
				map = (HashMap<String, ItemStack[]>)bois.readObject();
				bois.close();
				fis.close();
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
