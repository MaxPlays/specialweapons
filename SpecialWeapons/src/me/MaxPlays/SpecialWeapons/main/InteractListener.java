package me.MaxPlays.SpecialWeapons.main;

import java.util.HashMap;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.MaxPlays.SpecialWeapons.main.Items.Special;

/**
* Created by Max_Plays on 14.06.2017
*/
public class InteractListener implements Listener {

	static HashMap<Player, Long> map = new HashMap<>();
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) | e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			Player p = e.getPlayer();
			ItemStack is = e.getItem();
			if(is != null && Items.isSpecial(is)){
				if(Items.getSpecial(is).equals(Special.HA)){
					if(map.containsKey(p) && map.get(p) > System.currentTimeMillis()){
						p.sendMessage(SpecialWeapons.prefix + "You must wait �c" + ((Math.abs(System.currentTimeMillis() - map.get(p)) / 1000) + 1) + " �7seconds before using that again");
					}else{
						map.remove(p);
						map.put(p, System.currentTimeMillis() + 19*1000);
						for(int i = 0; i < 6; i++){
							if(p.getHealth() < 20)
								p.setHealth(p.getHealth() + 1);
						}
						p.playSound(p.getLocation(), Sound.VILLAGER_YES, 1, 2);
					}
				}
			}
		}
	}
	
}
