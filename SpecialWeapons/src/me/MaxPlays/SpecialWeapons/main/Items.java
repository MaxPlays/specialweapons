package me.MaxPlays.SpecialWeapons.main;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class Items {

	public static ItemStack ps, pa, ba, ha;
	
	public Items(){
		ps = new Stack(Material.DIAMOND_SWORD).setName("�cPoison Sword").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.FIRE_ASPECT, 2).setLore("�6Poison Effect", null, null)
				.build();
		pa = new Stack(Material.DIAMOND_AXE).setName("�cPoison Axe").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.DURABILITY, 2).setLore("�6Poison Effect", null, null)
				.build();
		ba = new Stack(Material.DIAMOND_AXE).setName("�cBlindness Axe").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.DURABILITY, 2).setLore("�6Blindness Effect", null, null)
				.build();
		ha = new Stack(Material.DIAMOND_AXE).setName("�cHealing Axe").addEnchantment(Enchantment.DAMAGE_ALL, 4)
				.addEnchantment(Enchantment.DURABILITY, 2).setLore("�6Heals you when you right click it", null, null)
				.build();
		
	}
	public static boolean isSpecial(ItemStack is){
		ItemStack i = new Stack(is).setAmount(1).setDurability(new ItemStack(is.getType()).getDurability()).build();
		if(i.equals(ps) | i.equals(pa) | i.equals(ba) | i.equals(ha)){
			return true;
		}
		return false;
	}
	
	public static Special getSpecial(ItemStack is){
		ItemStack i = new Stack(is).setAmount(1).setDurability(new ItemStack(is.getType()).getDurability()).build();
		if(isSpecial(i)){
			if(i.equals(ps))
				return Special.PS;
			if(i.equals(pa))
				return Special.PA;
			if(i.equals(ba))
				return Special.BA;
			if(i.equals(ha))
				return Special.HA;
		}
		return null;
	}
	
	public static enum Special{
		PS, PA, BA, HA;
	}
	
}


