package me.MaxPlays.SpecialWeapons.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class DropListener implements Listener {

	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		if(Items.isSpecial(e.getItemDrop().getItemStack()) && !p.isOp()){
			e.setCancelled(true);
			p.sendMessage(SpecialWeapons.prefix + "You can't drop that item. It could be very useful later");
		}
	}
	
}
