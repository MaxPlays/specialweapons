package me.MaxPlays.SpecialWeapons.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

/**
* Created by Max_Plays on 14.06.2017
*/
public class SpecialWeapons extends JavaPlugin{

	
	public static final String prefix = "�8[�cSpecialWeapons�8] �7";
	public static final String noperm = prefix + "�cYou don't have permission to do this";
	
	public static WorldGuardPlugin wg;
	
	public void onEnable(){
		
		DeathListener.load();
		
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new DeathListener(), this);
		pm.registerEvents(new HitListener(), this);
		pm.registerEvents(new DropListener(), this);
		pm.registerEvents(new InteractListener(), this);
		
		getCommand("poison-sword").setExecutor(new CommandWeapons());
		getCommand("poison-axe").setExecutor(new CommandWeapons());
		getCommand("blindness-axe").setExecutor(new CommandWeapons());
		getCommand("healing-axe").setExecutor(new CommandWeapons());
		
		new Items();
		
		Plugin pl = Bukkit.getPluginManager().getPlugin("WorldGuard");
		if(pl != null){
			wg = (WorldGuardPlugin) pl;
		}
		
	}
	public void onDisable(){
		DeathListener.save();
	}
	
}
