package me.MaxPlays.SpecialWeapons.main;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandWeapons implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player){
			
			Player p = (Player) sender;
			Player target = null;
			if(args.length == 0)
				target = p;
			if(args.length == 1){
				if(Bukkit.getPlayer(args[0]) != null){
					target = Bukkit.getPlayer(args[0]);
				}else{
					p.sendMessage(SpecialWeapons.prefix + "That player is not online");
					return true;
				}
			}
			if(args.length > 1){
				p.sendMessage(SpecialWeapons.prefix + "Too many arguments");
				return true;
			}
			String name = cmd.getName();
			if(name.equalsIgnoreCase("poison-sword")){
				if(p.hasPermission("p.s")){
					target.getInventory().addItem(Items.ps);
					target.sendMessage(SpecialWeapons.prefix + "There you go!");
					if(!p.equals(target))
						p.sendMessage(SpecialWeapons.prefix + "Item sent!");
				}else{
					p.sendMessage(SpecialWeapons.noperm);
				}
			}else if(name.equalsIgnoreCase("poison-axe")){
				if(p.hasPermission("p.a")){
					target.getInventory().addItem(Items.pa);
					target.sendMessage(SpecialWeapons.prefix + "There you go!");
					if(!p.equals(target))
						p.sendMessage(SpecialWeapons.prefix + "Item sent!");
				}else{
					p.sendMessage(SpecialWeapons.noperm);
				}
			}else if(name.equalsIgnoreCase("blindness-axe")){
				if(p.hasPermission("b.a")){
					target.getInventory().addItem(Items.ba);
					target.sendMessage(SpecialWeapons.prefix + "There you go!");
					if(!p.equals(target))
						p.sendMessage(SpecialWeapons.prefix + "Item sent!");
				}else{
					p.sendMessage(SpecialWeapons.noperm);
				}
				
			}else if(name.equalsIgnoreCase("healing-axe")){
				if(p.hasPermission("h.a")){
					target.getInventory().addItem(Items.ha);
					target.sendMessage(SpecialWeapons.prefix + "There you go!");
					if(!p.equals(target))
						p.sendMessage(SpecialWeapons.prefix + "Item sent!");
				}else{
					p.sendMessage(SpecialWeapons.noperm);
				}
			}
			
			
		}
		
		return true;
	}

}
